const cardTranslateData=[
        {"position":30},
        {"position":10},
        {"position":20}
    ];

    const iterationOrder=[0,2,1];
    const teamColors=["#1220C1","#525a60","#525a60"];
    const bannerClasses=["redbull","mercedes","mercedes"];
    const driverInfoTranslateData=[...Array(24).keys()]

    d3.selectAll("div.combo")
        .insert("svg")
        .attr("class","banner-svg")
        .style("background-color",function(d,i){ return teamColors[i];});


    d3.selectAll("svg.banner-svg")
        .append("svg:image")
        .attr("class",function(d,i){ return `${bannerClasses[i]}-logo shadow-basic`;})
        .attr("xlink:href", function(d,i){return `res/logos/${bannerClasses[i]}.png`});

    d3.selectAll("div.card")
        .data(cardTranslateData)
        .transition()
        .duration(800)
        .delay(function(d,i){
            return iterationOrder[i]*100;
        })
        .styleTween('transform', function (d,i) {
            return d3.interpolateString('translate(0px,0px)',`translate(0px,0px)`);
        });

    d3.selectAll("div.combo")
        .data(cardTranslateData)
        .transition()
        .duration(800)
        .delay(function(d,i){
            return iterationOrder[i]*100;
        })
        .style("opacity","1.0")
        .styleTween('transform', function (d,i) {
            return d3.interpolateString('translate(0px,0px)',`translate(0px,${-180+d.position}px)`)
        });

    

    let infos = d3.selectAll("span.driver-info-text")
        .data(driverInfoTranslateData)
        .transition()
        .duration(800)
        .delay(function(d,i){
            return 400 + iterationOrder[parseInt((i/8))]*100;})
        .style("opacity","1.0")
        .styleTween('transform', function (d,i) {
            return d3.interpolateString('translate(-5px,0px)',`translate(0px,0px)`)
        });
    

    d3.selectAll("div.driver-info-flex")
        .append("svg")
        .attr("class","svg-chart shadow-basic")
        .attr("id","chart")
        .style("margin-top",-240)
        .style("margin-left",175);

    d3.selectAll("div.driver-info-flex")
        .append("svg")
        .attr("id","legend")
        .style("margin-top",10)
        .style("margin-left",176)
        .transition()
        .delay(1900)
        .duration(200)
        .style("opacity",1.0);
    
    d3.selectAll("svg#legend")
        .selectAll("text")
        .data(["P1","P2","P3","Ret"])
        .enter()
        .append("text")
        .attr("class","legend-text")
        .text(function(d,i){return d;})
        .attr("y",20)
        .attr("x",function(d,i){return 40 + i*40;})

    let driverInfo=[
        {"color":["#000000","#1220C1","#3D63FF","#5593FF"],"win-data":[5,3,6,2]},       // Data taken from wikis linked on index.html -- Season 2020 data
        {"color":["#000000","#525a60","#677178","#838d94"],"win-data":[0,2,1,11]},
        {"color":["#000000","#525a60","#677178","#838d94"],"win-data":[1,3,6,2]},
        {"color":[],"win-data":[]}
    ]
    
    let iCount=0;

    d3.selectAll("svg#legend")
        .selectAll("rect")
        .data(driverInfo)
        .enter()
        .append("rect")
        .attr("width",10)
        .attr("height",10)
        .attr("y",10)
        .attr("x",function(d,i){return 28 + i*40;})
        .style("fill",function(d,i){
            return driverInfo[parseInt((iCount++)/4)].color[3-i];
        });

   

    createPieCharts();              // https://codepen.io/jongeorge1/pen/jONqjad

    function createPieCharts() {
        d3.selectAll("#chart").html("");

        let sizes = {
            innerRadius: 50,
            outerRadius: 100
        };

        let durations = {
            entryAnimation: 1000
        };
        
        let generator = d3.pie()
            .sort(null);

        let chart=[];
        for(let i=0;i<4;i++){                                       // Generating objects with necessary data for pie chart display and animation (startAngle, endAngle)
            chart.push(generator(driverInfo[i]["win-data"]));
        }

        let iCount=0;
        let arcs = d3.selectAll("#chart")
            .append("g")
            .attr("transform", "translate(100, 100)")
            .selectAll("path")
            .data(driverInfo)
            .enter()
            .append("path")
            .style("fill", function(d,i){
                return driverInfo[parseInt((iCount++)/4)].color[i];
            });

        let angleInterpolation = d3.interpolate(generator.startAngle()(), generator.endAngle()());

        let arc = d3.arc()
            .innerRadius(sizes.innerRadius)
            .outerRadius(sizes.outerRadius);

            iCount=0;
        arcs.data(chart)
            .transition()
            .delay(900)
            .duration(durations.entryAnimation)
            .attrTween("d", function(d,i) {
            let currentData = chart[parseInt((iCount++)/4)][i];

            let originalEnd = currentData.endAngle;
            return t => {
                let currentAngle = angleInterpolation(t);
                if (currentAngle < currentData.startAngle) {
                    return "";
                }

                currentData.endAngle = Math.min(currentAngle, originalEnd);

                return arc(currentData);
            };
            });

        iCount = 0;
        let jCount=0;
        d3.selectAll("g")                   // https://codepen.io/MeredithU/pen/OVMjjK
            .selectAll("text")
            .data(chart)
            .enter()
            .append("text")
            .attr("transform", function(d,i) {
                let currentData = chart[parseInt((iCount++)/4)][i];
                if(iCount>=12){
                    iCount=0;
                }
                return `translate(${arc.centroid(currentData)})`;   // Objects made with generator contain data needed for positioning text in pie charts with arc.centroid(object)
            })
            .attr("class","chart-text")
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .style("fill",function(d,i){
                return "#F7F4F1";
            })
            .text(function(d,i) { 
                let text = driverInfo[parseInt((jCount++)/4)]["win-data"][i];
                if(text=="0"){
                    text="";
                }
                return text; })
            .transition()
            .duration(200)
            .delay(1900)
            .style("opacity",1.0)
            .style("text-shadow","0.5px 0.5px 0.5px black");
    }