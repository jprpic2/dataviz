let data = [{"Year": 2020,"Position": 1,"DriverNumber": 77,"Venue": "Austria","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:04.111","Q2": "1:03.015","Q3": "1:02.939"},
        {"Year": 2020,"Position": 2,"DriverNumber": 44,"Venue": "Austria","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:04.198","Q2": "1:03.096","Q3": "1:02.951"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Austria","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:04.024","Q2": "1:04.000","Q3": "1:03.477"},
        {"Year": 2020,"Position": 4,"DriverNumber": 4,"Venue": "Austria","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:04.606","Q2": "1:03.819","Q3": "1:03.626"},
        {"Year": 2020,"Position": 5,"DriverNumber": 23,"Venue": "Austria","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:04.661","Q2": "1:03.746","Q3": "1:03.868"},
        {"Year": 2020,"Position": 6,"DriverNumber": 11,"Venue": "Austria","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:04.543","Q2": "1:03.860","Q3": "1:03.868"},
        {"Year": 2020,"Position": 7,"DriverNumber": 16,"Venue": "Austria","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:04.500","Q2": "1:04.041","Q3": "1:03.923"},
        {"Year": 2020,"Position": 8,"DriverNumber": 55,"Venue": "Austria","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:04.537","Q2": "1:03.971","Q3": "1:03.971"},
        {"Year": 2020,"Position": 9,"DriverNumber": 18,"Venue": "Austria","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:04.309","Q2": "1:03.955","Q3": "1:04.029"},
        {"Year": 2020,"Position": 10,"DriverNumber": 3,"Venue": "Austria","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:04.556","Q2": "1:04.023","Q3": "1:04.239"},
        {"Year": 2020,"Position": 11,"DriverNumber": 5,"Venue": "Austria","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:04.554","Q2": "1:04.206","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 10,"Venue": "Austria","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:04.603","Q2": "1:04.305","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 26,"Venue": "Austria","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:05.031","Q2": "1:04.431","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 31,"Venue": "Austria","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:04.933","Q2": "1:04.643","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 8,"Venue": "Austria","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:05.094","Q2": "1:04.691","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Austria","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:05.164","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 63,"Venue": "Austria","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:05.167","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 99,"Venue": "Austria","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:05.175","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 7,"Venue": "Austria","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:05.224","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Austria","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:05.757","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Styria","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:18.188","Q2": "1:17.825","Q3": "1:19.273"},
        {"Year": 2020,"Position": 2,"DriverNumber": 33,"Venue": "Styria","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:18.297","Q2": "1:17.938","Q3": "1:20.489"},
        {"Year": 2020,"Position": 3,"DriverNumber": 55,"Venue": "Styria","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:18.590","Q2": "1:18.836","Q3": "1:20.671"},
        {"Year": 2020,"Position": 4,"DriverNumber": 77,"Venue": "Styria","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:18.791","Q2": "1:18.657","Q3": "1:20.701"},
        {"Year": 2020,"Position": 5,"DriverNumber": 31,"Venue": "Styria","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:19.687","Q2": "1:18.764","Q3": "1:20.922"},
        {"Year": 2020,"Position": 6,"DriverNumber": 4,"Venue": "Styria","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:18.504","Q2": "1:18.448","Q3": "1:20.925"},
        {"Year": 2020,"Position": 7,"DriverNumber": 23,"Venue": "Styria","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:20.882","Q2": "1:19.014","Q3": "1:21.011"},
        {"Year": 2020,"Position": 8,"DriverNumber": 10,"Venue": "Styria","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:20.192","Q2": "1:18.744","Q3": "1:21.028"},
        {"Year": 2020,"Position": 9,"DriverNumber": 3,"Venue": "Styria","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:19.662","Q2": "1:19.229","Q3": "1:21.192"},
        {"Year": 2020,"Position": 10,"DriverNumber": 5,"Venue": "Styria","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:20.243","Q2": "1:19.545","Q3": "1:21.651"},
        {"Year": 2020,"Position": 11,"DriverNumber": 16,"Venue": "Styria","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:20.871","Q2": "1:19.628","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 63,"Venue": "Styria","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:20.382","Q2": "1:19.636","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 18,"Venue": "Styria","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:19.697","Q2": "1:19.645","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 26,"Venue": "Styria","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:19.824","Q2": "1:19.717","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 20,"Venue": "Styria","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:21.140","Q2": "1:20.211","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 7,"Venue": "Styria","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:21.372","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 11,"Venue": "Styria","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:21.607","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 6,"Venue": "Styria","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:21.759","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 99,"Venue": "Styria","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:21.831","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 8,"Venue": "Styria","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "DNS","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Hungary","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:14.907","Q2": "1:14.261","Q3": "1:13.447"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Hungary","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:15.474","Q2": "1:14.530","Q3": "1:13.554"},
        {"Year": 2020,"Position": 3,"DriverNumber": 18,"Venue": "Hungary","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:14.895","Q2": "1:15.176","Q3": "1:14.377"},
        {"Year": 2020,"Position": 4,"DriverNumber": 11,"Venue": "Hungary","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:14.681","Q2": "1:15.394","Q3": "1:14.545"},
        {"Year": 2020,"Position": 5,"DriverNumber": 5,"Venue": "Hungary","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:15.455","Q2": "1:15.131","Q3": "1:14.774"},
        {"Year": 2020,"Position": 6,"DriverNumber": 16,"Venue": "Hungary","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:15.793","Q2": "1:15.006","Q3": "1:14.817"},
        {"Year": 2020,"Position": 7,"DriverNumber": 33,"Venue": "Hungary","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:15.495","Q2": "1:14.976","Q3": "1:14.849"},
        {"Year": 2020,"Position": 8,"DriverNumber": 4,"Venue": "Hungary","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:15.444","Q2": "1:15.085","Q3": "1:14.966"},
        {"Year": 2020,"Position": 9,"DriverNumber": 55,"Venue": "Hungary","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:15.281","Q2": "1:15.267","Q3": "1:15.027"},
        {"Year": 2020,"Position": 10,"DriverNumber": 10,"Venue": "Hungary","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:15.767","Q2": "1:15.508","Q3": "N/A"},
        {"Year": 2020,"Position": 11,"DriverNumber": 3,"Venue": "Hungary","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:15.848","Q2": "1:15.661","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 63,"Venue": "Hungary","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:15.585","Q2": "1:15.698","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 23,"Venue": "Hungary","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:15.722","Q2": "1:15.715","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 31,"Venue": "Hungary","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:15.719","Q2": "1:15.742","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 6,"Venue": "Hungary","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:16.105","Q2": "1:16.544","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Hungary","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:16.152","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 26,"Venue": "Hungary","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:16.204","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 8,"Venue": "Hungary","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:16.407","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 99,"Venue": "Hungary","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:16.506","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 7,"Venue": "Hungary","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:16.614","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Great-Britain","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:25.900","Q2": "1:25.347","Q3": "1:24.303"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Great-Britain","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:25.801","Q2": "1:25.015","Q3": "1:24.616"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Great-Britain","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:26.115","Q2": "1:26.144","Q3": "1:25.325"},
        {"Year": 2020,"Position": 4,"DriverNumber": 16,"Venue": "Great-Britain","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:26.550","Q2": "1:26.203","Q3": "1:25.427"},
        {"Year": 2020,"Position": 5,"DriverNumber": 4,"Venue": "Great-Britain","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:26.855","Q2": "1:26.420","Q3": "1:25.782"},
        {"Year": 2020,"Position": 6,"DriverNumber": 18,"Venue": "Great-Britain","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:26.243","Q2": "1:26.501","Q3": "1:25.839"},
        {"Year": 2020,"Position": 7,"DriverNumber": 55,"Venue": "Great-Britain","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:26.715","Q2": "1:26.149","Q3": "1:25.965"},
        {"Year": 2020,"Position": 8,"DriverNumber": 3,"Venue": "Great-Britain","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:26.677","Q2": "1:26.339","Q3": "1:26.009"},
        {"Year": 2020,"Position": 9,"DriverNumber": 31,"Venue": "Great-Britain","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:26.396","Q2": "1:26.252","Q3": "1:26.209"},
        {"Year": 2020,"Position": 10,"DriverNumber": 5,"Venue": "Great-Britain","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:26.469","Q2": "1:26.455","Q3": "1:26.339"},
        {"Year": 2020,"Position": 11,"DriverNumber": 10,"Venue": "Great-Britain","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:26.343","Q2": "1:26.501","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 23,"Venue": "Great-Britain","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:26.565","Q2": "1:26.545","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 27,"Venue": "Great-Britain","Name": "Nico Hulkenberg","Abbreviation": "HUL","Team": "Racing Point BWT Mercedes","Q1": "1:26.327","Q2": "1:26.566","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 26,"Venue": "Great-Britain","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:26.774","Q2": "1:26.744","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 63,"Venue": "Great-Britain","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:26.732","Q2": "1:27.092","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Great-Britain","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:27.158","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 99,"Venue": "Great-Britain","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:27.164","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 7,"Venue": "Great-Britain","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:27.366","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 8,"Venue": "Great-Britain","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:27.643","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Great-Britain","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:27.705","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 77,"Venue": "70th Anniversary","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:26.738","Q2": "1:25.785","Q3": "1:25.154"},
        {"Year": 2020,"Position": 2,"DriverNumber": 44,"Venue": "70th Anniversary","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:26.818","Q2": "1:26.266","Q3": "1:25.217"},
        {"Year": 2020,"Position": 3,"DriverNumber": 27,"Venue": "70th Anniversary","Name": "Nico Hulkenberg","Abbreviation": "HUL","Team": "Racing Point BWT Mercedes","Q1": "1:27.279","Q2": "1:26.261","Q3": "1:26.082"},
        {"Year": 2020,"Position": 4,"DriverNumber": 33,"Venue": "70th Anniversary","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:27.154","Q2": "1:26.779","Q3": "1:26.176"},
        {"Year": 2020,"Position": 5,"DriverNumber": 3,"Venue": "70th Anniversary","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:27.442","Q2": "1:26.636","Q3": "1:26.297"},
        {"Year": 2020,"Position": 6,"DriverNumber": 18,"Venue": "70th Anniversary","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:27.187","Q2": "1:26.674","Q3": "1:26.428"},
        {"Year": 2020,"Position": 7,"DriverNumber": 10,"Venue": "70th Anniversary","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:27.154","Q2": "1:26.523","Q3": "1:26.534"},
        {"Year": 2020,"Position": 8,"DriverNumber": 16,"Venue": "70th Anniversary","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:27.427","Q2": "1:26.709","Q3": "1:26.614"},
        {"Year": 2020,"Position": 9,"DriverNumber": 23,"Venue": "70th Anniversary","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:27.153","Q2": "1:26.642","Q3": "1:26.669"},
        {"Year": 2020,"Position": 10,"DriverNumber": 4,"Venue": "70th Anniversary","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:27.217","Q2": "1:26.885","Q3": "1:26.778"},
        {"Year": 2020,"Position": 11,"DriverNumber": 31,"Venue": "70th Anniversary","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:27.278","Q2": "1:27.011","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 5,"Venue": "70th Anniversary","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:27.612","Q2": "1:27.078","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 55,"Venue": "70th Anniversary","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:27.450","Q2": "1:27.083","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 8,"Venue": "70th Anniversary","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:27.519","Q2": "1:27.254","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 63,"Venue": "70th Anniversary","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:27.757","Q2": "1:27.455","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 26,"Venue": "70th Anniversary","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:27.882","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 20,"Venue": "70th Anniversary","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:28.236","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 6,"Venue": "70th Anniversary","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:28.430","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 99,"Venue": "70th Anniversary","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:28.433","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 7,"Venue": "70th Anniversary","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:28.493","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Spanish","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:16.872","Q2": "1:16.013","Q3": "1:15.584"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Spanish","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:17.243","Q2": "1:16.152","Q3": "1:15.643"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Spanish","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:17.213","Q2": "1:16.518","Q3": "1:16.292"},
        {"Year": 2020,"Position": 4,"DriverNumber": 11,"Venue": "Spanish","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:17.117","Q2": "1:16.936","Q3": "1:16.482"},
        {"Year": 2020,"Position": 5,"DriverNumber": 18,"Venue": "Spanish","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:17.316","Q2": "1:16.666","Q3": "1:16.589"},
        {"Year": 2020,"Position": 6,"DriverNumber": 23,"Venue": "Spanish","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:17.419","Q2": "1:17.163","Q3": "1:17.029"},
        {"Year": 2020,"Position": 7,"DriverNumber": 55,"Venue": "Spanish","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:17.438","Q2": "1:16.876","Q3": "1:17.044"},
        {"Year": 2020,"Position": 8,"DriverNumber": 4,"Venue": "Spanish","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:17.577","Q2": "1:17.166","Q3": "1:17.084"},
        {"Year": 2020,"Position": 9,"DriverNumber": 16,"Venue": "Spanish","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:17.256","Q2": "1:16.953","Q3": "1:17.087"},
        {"Year": 2020,"Position": 10,"DriverNumber": 10,"Venue": "Spanish","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:17.356","Q2": "1:16.800","Q3": "1:17.136"},
        {"Year": 2020,"Position": 11,"DriverNumber": 5,"Venue": "Spanish","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:17.573","Q2": "1:17.168","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 26,"Venue": "Spanish","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:17.676","Q2": "1:17.192","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 3,"Venue": "Spanish","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:17.667","Q2": "1:17.198","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 7,"Venue": "Spanish","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:17.797","Q2": "1:17.386","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 31,"Venue": "Spanish","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:17.765","Q2": "1:17.567","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Spanish","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:17.908","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 8,"Venue": "Spanish","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:18.089","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 63,"Venue": "Spanish","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:18.099","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 6,"Venue": "Spanish","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:18.532","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 99,"Venue": "Spanish","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:18.697","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Belgium","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:42.323","Q2": "1:42.014","Q3": "1:41.252"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Belgium","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:42.534","Q2": "1:42.126","Q3": "1:41.763"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Belgium","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:43.197","Q2": "1:42.473","Q3": "1:41.778"},
        {"Year": 2020,"Position": 4,"DriverNumber": 3,"Venue": "Belgium","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:43.309","Q2": "1:42.487","Q3": "1:42.061"},
        {"Year": 2020,"Position": 5,"DriverNumber": 23,"Venue": "Belgium","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:43.418","Q2": "1:42.193","Q3": "1:42.264"},
        {"Year": 2020,"Position": 6,"DriverNumber": 31,"Venue": "Belgium","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:43.505","Q2": "1:42.534","Q3": "1:42.396"},
        {"Year": 2020,"Position": 7,"DriverNumber": 55,"Venue": "Belgium","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:43.322","Q2": "1:42.478","Q3": "1:42.438"},
        {"Year": 2020,"Position": 8,"DriverNumber": 11,"Venue": "Belgium","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:43.349","Q2": "1:42.670","Q3": "1:42.532"},
        {"Year": 2020,"Position": 9,"DriverNumber": 18,"Venue": "Belgium","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:43.265","Q2": "1:42.491","Q3": "1:42.603"},
        {"Year": 2020,"Position": 10,"DriverNumber": 4,"Venue": "Belgium","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:43.514","Q2": "1:42.722","Q3": "1:42.657"},
        {"Year": 2020,"Position": 11,"DriverNumber": 26,"Venue": "Belgium","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:43.267","Q2": "1:42.730","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 10,"Venue": "Belgium","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:43.262","Q2": "1:42.745","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 16,"Venue": "Belgium","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:43.656","Q2": "1:42.996","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 5,"Venue": "Belgium","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:43.567","Q2": "1:43.261","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 63,"Venue": "Belgium","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:43.630","Q2": "1:43.468","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 7,"Venue": "Belgium","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:43.743","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 8,"Venue": "Belgium","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:43.838","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 99,"Venue": "Belgium","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:43.950","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 6,"Venue": "Belgium","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:44.138","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 20,"Venue": "Belgium","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:44.314","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Italy","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:19.514","Q2": "1:19.092","Q3": "1:18.887"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Italy","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:19.786","Q2": "1:18.952","Q3": "1:18.956"},
        {"Year": 2020,"Position": 3,"DriverNumber": 55,"Venue": "Italy","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:20.099","Q2": "1:19.705","Q3": "1:19.695"},
        {"Year": 2020,"Position": 4,"DriverNumber": 11,"Venue": "Italy","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:20.048","Q2": "1:19.718","Q3": "1:19.720"},
        {"Year": 2020,"Position": 5,"DriverNumber": 33,"Venue": "Italy","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:20.193","Q2": "1:19.780","Q3": "1:19.795"},
        {"Year": 2020,"Position": 6,"DriverNumber": 4,"Venue": "Italy","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:20.344","Q2": "1:19.962","Q3": "1:19.820"},
        {"Year": 2020,"Position": 7,"DriverNumber": 3,"Venue": "Italy","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:20.548","Q2": "1:20.031","Q3": "1:19.864"},
        {"Year": 2020,"Position": 8,"DriverNumber": 18,"Venue": "Italy","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:20.400","Q2": "1:19.924","Q3": "1:20.049"},
        {"Year": 2020,"Position": 9,"DriverNumber": 23,"Venue": "Italy","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:21.104","Q2": "1:20.064","Q3": "1:20.090"},
        {"Year": 2020,"Position": 10,"DriverNumber": 10,"Venue": "Italy","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:20.145","Q2": "1:19.909","Q3": "1:20.177"},
        {"Year": 2020,"Position": 11,"DriverNumber": 26,"Venue": "Italy","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:20.307","Q2": "1:20.169","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 31,"Venue": "Italy","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:20.747","Q2": "1:20.234","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 16,"Venue": "Italy","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:20.443","Q2": "1:20.273","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 7,"Venue": "Italy","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:21.010","Q2": "1:20.926","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 20,"Venue": "Italy","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:20.869","Q2": "1:21.573","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 8,"Venue": "Italy","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:21.139","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 5,"Venue": "Italy","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:21.151","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 99,"Venue": "Italy","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:21.206","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 63,"Venue": "Italy","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:21.587","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Italy","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:21.717","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Tuscany","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:15.778","Q2": "1:15.309","Q3": "1:15.144"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Tuscany","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:15.749","Q2": "1:15.322","Q3": "1:15.203"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Tuscany","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:16.335","Q2": "1:15.471","Q3": "1:15.509"},
        {"Year": 2020,"Position": 4,"DriverNumber": 23,"Venue": "Tuscany","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:16.527","Q2": "1:15.914","Q3": "1:15.954"},
        {"Year": 2020,"Position": 5,"DriverNumber": 16,"Venue": "Tuscany","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:16.698","Q2": "1:16.324","Q3": "1:16.270"},
        {"Year": 2020,"Position": 6,"DriverNumber": 11,"Venue": "Tuscany","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:16.596","Q2": "1:16.489","Q3": "1:16.311"},
        {"Year": 2020,"Position": 7,"DriverNumber": 18,"Venue": "Tuscany","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:16.701","Q2": "1:16.271","Q3": "1:16.356"},
        {"Year": 2020,"Position": 8,"DriverNumber": 3,"Venue": "Tuscany","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:16.981","Q2": "1:16.243","Q3": "1:16.543"},
        {"Year": 2020,"Position": 9,"DriverNumber": 55,"Venue": "Tuscany","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:16.993","Q2": "1:16.522","Q3": "1:17.870"},
        {"Year": 2020,"Position": 10,"DriverNumber": 31,"Venue": "Tuscany","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:16.825","Q2": "1:16.297","Q3": "DNF"},
        {"Year": 2020,"Position": 11,"DriverNumber": 4,"Venue": "Tuscany","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:16.895","Q2": "1:16.640","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 26,"Venue": "Tuscany","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:16.928","Q2": "1:16.854","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 7,"Venue": "Tuscany","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:17.059","Q2": "1:16.854","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 5,"Venue": "Tuscany","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:17.072","Q2": "1:16.858","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 8,"Venue": "Tuscany","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:17.069","Q2": "1:17.254","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 10,"Venue": "Tuscany","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:17.125","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 99,"Venue": "Tuscany","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:17.220","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 63,"Venue": "Tuscany","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:17.232","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 6,"Venue": "Tuscany","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:17.320","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 20,"Venue": "Tuscany","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:17.348","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Russia","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:32.983","Q2": "1:32.835","Q3": "1:31.304"},
        {"Year": 2020,"Position": 2,"DriverNumber": 33,"Venue": "Russia","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:33.630","Q2": "1:33.157","Q3": "1:31.867"},
        {"Year": 2020,"Position": 3,"DriverNumber": 77,"Venue": "Russia","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:32.656","Q2": "1:32.405","Q3": "1:31.956"},
        {"Year": 2020,"Position": 4,"DriverNumber": 11,"Venue": "Russia","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:33.704","Q2": "1:33.038","Q3": "1:32.317"},
        {"Year": 2020,"Position": 5,"DriverNumber": 3,"Venue": "Russia","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:33.650","Q2": "1:32.218","Q3": "1:32.364"},
        {"Year": 2020,"Position": 6,"DriverNumber": 55,"Venue": "Russia","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:33.967","Q2": "1:32.757","Q3": "1:32.550"},
        {"Year": 2020,"Position": 7,"DriverNumber": 31,"Venue": "Russia","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:33.557","Q2": "1:33.196","Q3": "1:32.624"},
        {"Year": 2020,"Position": 8,"DriverNumber": 4,"Venue": "Russia","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:33.804","Q2": "1:33.081","Q3": "1:32.847"},
        {"Year": 2020,"Position": 9,"DriverNumber": 10,"Venue": "Russia","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:33.734","Q2": "1:33.139","Q3": "1:33.000"},
        {"Year": 2020,"Position": 10,"DriverNumber": 23,"Venue": "Russia","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:33.919","Q2": "1:33.153","Q3": "1:33.008"},
        {"Year": 2020,"Position": 11,"DriverNumber": 16,"Venue": "Russia","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:34.071","Q2": "1:33.239","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 26,"Venue": "Russia","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:33.511","Q2": "1:33.249","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 18,"Venue": "Russia","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:33.852","Q2": "1:33.364","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 63,"Venue": "Russia","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:34.020","Q2": "1:33.583","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 5,"Venue": "Russia","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:34.134","Q2": "1:33.609","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 8,"Venue": "Russia","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:34.592","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 99,"Venue": "Russia","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:34.594","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 20,"Venue": "Russia","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:34.681","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 6,"Venue": "Russia","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:35.066","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 7,"Venue": "Russia","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:35.267","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 77,"Venue": "Eifel","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:26.573","Q2": "1:25.971","Q3": "1:25.269"},
        {"Year": 2020,"Position": 2,"DriverNumber": 44,"Venue": "Eifel","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:26.620","Q2": "1:25.390","Q3": "1:25.525"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Eifel","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:26.319","Q2": "1:25.467","Q3": "1:25.562"},
        {"Year": 2020,"Position": 4,"DriverNumber": 16,"Venue": "Eifel","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:26.857","Q2": "1:26.240","Q3": "1:26.035"},
        {"Year": 2020,"Position": 5,"DriverNumber": 23,"Venue": "Eifel","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:27.126","Q2": "1:26.285","Q3": "1:26.047"},
        {"Year": 2020,"Position": 6,"DriverNumber": 3,"Venue": "Eifel","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:26.836","Q2": "1:26.096","Q3": "1:26.223"},
        {"Year": 2020,"Position": 7,"DriverNumber": 31,"Venue": "Eifel","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:27.086","Q2": "1:26.364","Q3": "1:26.242"},
        {"Year": 2020,"Position": 8,"DriverNumber": 4,"Venue": "Eifel","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:26.829","Q2": "1:26.316","Q3": "1:26.458"},
        {"Year": 2020,"Position": 9,"DriverNumber": 11,"Venue": "Eifel","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:27.120","Q2": "1:26.330","Q3": "1:26.704"},
        {"Year": 2020,"Position": 10,"DriverNumber": 55,"Venue": "Eifel","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:27.378","Q2": "1:26.361","Q3": "1:26.709"},
        {"Year": 2020,"Position": 11,"DriverNumber": 5,"Venue": "Eifel","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:27.107","Q2": "1:26.738","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 10,"Venue": "Eifel","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:27.072","Q2": "1:26.776","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 26,"Venue": "Eifel","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:27.285","Q2": "1:26.848","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 99,"Venue": "Eifel","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:27.532","Q2": "1:26.936","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 20,"Venue": "Eifel","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:27.231","Q2": "1:27.125","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 8,"Venue": "Eifel","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:27.552","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 63,"Venue": "Eifel","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:27.564","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 6,"Venue": "Eifel","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:27.812","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 7,"Venue": "Eifel","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:27.817","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 27,"Venue": "Eifel","Name": "Nico Hulkenberg","Abbreviation": "HUL","Team": "Racing Point BWT Mercedes","Q1": "1:28.021","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Portugal","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:16.828","Q2": "1:16.824","Q3": "1:16.652"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Portugal","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:16.945","Q2": "1:16.466","Q3": "1:16.754"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Portugal","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:16.879","Q2": "1:17.038","Q3": "1:16.904"},
        {"Year": 2020,"Position": 4,"DriverNumber": 16,"Venue": "Portugal","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:17.421","Q2": "1:17.367","Q3": "1:17.090"},
        {"Year": 2020,"Position": 5,"DriverNumber": 11,"Venue": "Portugal","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:17.370","Q2": "1:17.129","Q3": "1:17.223"},
        {"Year": 2020,"Position": 6,"DriverNumber": 23,"Venue": "Portugal","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:17.435","Q2": "1:17.411","Q3": "1:17.437"},
        {"Year": 2020,"Position": 7,"DriverNumber": 55,"Venue": "Portugal","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:17.627","Q2": "1:17.183","Q3": "1:17.520"},
        {"Year": 2020,"Position": 8,"DriverNumber": 4,"Venue": "Portugal","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:17.547","Q2": "1:17.321","Q3": "1:17.525"},
        {"Year": 2020,"Position": 9,"DriverNumber": 10,"Venue": "Portugal","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:17.209","Q2": "1:17.367","Q3": "1:17.803"},
        {"Year": 2020,"Position": 10,"DriverNumber": 3,"Venue": "Portugal","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:17.621","Q2": "1:17.481","Q3": "N/A"},
        {"Year": 2020,"Position": 11,"DriverNumber": 31,"Venue": "Portugal","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:17.775","Q2": "1:17.614","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 18,"Venue": "Portugal","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:17.667","Q2": "1:17.626","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 26,"Venue": "Portugal","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:17.841","Q2": "1:17.728","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 63,"Venue": "Portugal","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:17.931","Q2": "1:17.788","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 5,"Venue": "Portugal","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:17.446","Q2": "1:17.919","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 7,"Venue": "Portugal","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:18.201","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 99,"Venue": "Portugal","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:18.323","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 8,"Venue": "Portugal","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:18.364","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 20,"Venue": "Portugal","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:18.508","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Portugal","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:18.777","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 77,"Venue": "Emilia-Romagna","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:14.221","Q2": "1:14.585","Q3": "1:13.609"},
        {"Year": 2020,"Position": 2,"DriverNumber": 44,"Venue": "Emilia-Romagna","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:14.229","Q2": "1:14.643","Q3": "1:13.706"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Emilia-Romagna","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:15.034","Q2": "1:14.974","Q3": "1:14.176"},
        {"Year": 2020,"Position": 4,"DriverNumber": 10,"Venue": "Emilia-Romagna","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:15.183","Q2": "1:14.681","Q3": "1:14.502"},
        {"Year": 2020,"Position": 5,"DriverNumber": 3,"Venue": "Emilia-Romagna","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:15.474","Q2": "1:14.953","Q3": "1:14.520"},
        {"Year": 2020,"Position": 6,"DriverNumber": 23,"Venue": "Emilia-Romagna","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:15.402","Q2": "1:14.745","Q3": "1:14.572"},
        {"Year": 2020,"Position": 7,"DriverNumber": 16,"Venue": "Emilia-Romagna","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:15.123","Q2": "1:15.017","Q3": "1:14.616"},
        {"Year": 2020,"Position": 8,"DriverNumber": 26,"Venue": "Emilia-Romagna","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:15.412","Q2": "1:15.022","Q3": "1:14.696"},
        {"Year": 2020,"Position": 9,"DriverNumber": 4,"Venue": "Emilia-Romagna","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:15.274","Q2": "1:15.051","Q3": "1:14.814"},
        {"Year": 2020,"Position": 10,"DriverNumber": 55,"Venue": "Emilia-Romagna","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:15.528","Q2": "1:15.027","Q3": "1:14.911"},
        {"Year": 2020,"Position": 11,"DriverNumber": 11,"Venue": "Emilia-Romagna","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:15.407","Q2": "1:15.061","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 31,"Venue": "Emilia-Romagna","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:15.352","Q2": "1:15.201","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 63,"Venue": "Emilia-Romagna","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:15.760","Q2": "1:15.323","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 5,"Venue": "Emilia-Romagna","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:15.571","Q2": "1:15.385","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 18,"Venue": "Emilia-Romagna","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:15.822","Q2": "1:15.494","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 8,"Venue": "Emilia-Romagna","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:15.918","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 20,"Venue": "Emilia-Romagna","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:15.939","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 7,"Venue": "Emilia-Romagna","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:15.953","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 6,"Venue": "Emilia-Romagna","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:15.987","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 99,"Venue": "Emilia-Romagna","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:16.208","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 18,"Venue": "Turkey","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "2:07.467","Q2": "1:53.372","Q3": "1:47.765"},
        {"Year": 2020,"Position": 2,"DriverNumber": 33,"Venue": "Turkey","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:57.485","Q2": "1:50.293","Q3": "1:48.055"},
        {"Year": 2020,"Position": 3,"DriverNumber": 11,"Venue": "Turkey","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "2:07.614","Q2": "1:54.097","Q3": "1:49.321"},
        {"Year": 2020,"Position": 4,"DriverNumber": 23,"Venue": "Turkey","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:59.431","Q2": "1:52.282","Q3": "1:50.448"},
        {"Year": 2020,"Position": 5,"DriverNumber": 3,"Venue": "Turkey","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "2:05.598","Q2": "1:54.278","Q3": "1:51.595"},
        {"Year": 2020,"Position": 6,"DriverNumber": 44,"Venue": "Turkey","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "2:07.599","Q2": "1:52.709","Q3": "1:52.560"},
        {"Year": 2020,"Position": 7,"DriverNumber": 31,"Venue": "Turkey","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "2:06.115","Q2": "1:53.657","Q3": "1:52.622"},
        {"Year": 2020,"Position": 8,"DriverNumber": 7,"Venue": "Turkey","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "2:01.249","Q2": "1:53.793","Q3": "1:52.745"},
        {"Year": 2020,"Position": 9,"DriverNumber": 77,"Venue": "Turkey","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "2:07.001","Q2": "1:53.767","Q3": "1:53.258"},
        {"Year": 2020,"Position": 10,"DriverNumber": 99,"Venue": "Turkey","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "2:07.341","Q2": "1:53.431","Q3": "1:57.226"},
        {"Year": 2020,"Position": 11,"DriverNumber": 4,"Venue": "Turkey","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "2:07.167","Q2": "1:54.945","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 5,"Venue": "Turkey","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "2:03.356","Q2": "1:55.169","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 55,"Venue": "Turkey","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "2:07.489","Q2": "1:55.410","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 16,"Venue": "Turkey","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "2:04.464","Q2": "1:56.696","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 10,"Venue": "Turkey","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "2:05.579","Q2": "1:58.556","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Turkey","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "2:08.007","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 26,"Venue": "Turkey","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "2:09.070","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 63,"Venue": "Turkey","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "2:10.017","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 8,"Venue": "Turkey","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "2:12.909","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Turkey","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "2:21.611","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 44,"Venue": "Bahrain","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:28.343","Q2": "1:27.586","Q3": "1:27.264"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Bahrain","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:28.767","Q2": "1:28.063","Q3": "1:27.553"},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Bahrain","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:28.885","Q2": "1:28.025","Q3": "1:27.678"},
        {"Year": 2020,"Position": 4,"DriverNumber": 23,"Venue": "Bahrain","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:28.732","Q2": "1:28.749","Q3": "1:28.274"},
        {"Year": 2020,"Position": 5,"DriverNumber": 11,"Venue": "Bahrain","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:29.178","Q2": "1:28.894","Q3": "1:28.322"},
        {"Year": 2020,"Position": 6,"DriverNumber": 3,"Venue": "Bahrain","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:29.005","Q2": "1:28.648","Q3": "1:28.417"},
        {"Year": 2020,"Position": 7,"DriverNumber": 31,"Venue": "Bahrain","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:29.203","Q2": "1:28.937","Q3": "1:28.419"},
        {"Year": 2020,"Position": 8,"DriverNumber": 10,"Venue": "Bahrain","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:28.971","Q2": "1:29.008","Q3": "1:28.448"},
        {"Year": 2020,"Position": 9,"DriverNumber": 4,"Venue": "Bahrain","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:29.464","Q2": "1:28.877","Q3": "1:28.542"},
        {"Year": 2020,"Position": 10,"DriverNumber": 26,"Venue": "Bahrain","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:29.158","Q2": "1:28.944","Q3": "1:28.618"},
        {"Year": 2020,"Position": 11,"DriverNumber": 5,"Venue": "Bahrain","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:29.142","Q2": "1:29.149","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 16,"Venue": "Bahrain","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:29.137","Q2": "1:29.165","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 18,"Venue": "Bahrain","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:28.679","Q2": "1:29.557","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 63,"Venue": "Bahrain","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:29.294","Q2": "1:31.218","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 55,"Venue": "Bahrain","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:28.975","Q2": "DNF","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 99,"Venue": "Bahrain","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:29.491","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 7,"Venue": "Bahrain","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:29.810","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 20,"Venue": "Bahrain","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:30.111","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 8,"Venue": "Bahrain","Name": "Romain Grosjean","Abbreviation": "GRO","Team": "Haas Ferrari","Q1": "1:30.138","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Bahrain","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:30.182","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 77,"Venue": "Sakhir","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": 53.904,"Q2": 53.803,"Q3": 53.377},
        {"Year": 2020,"Position": 2,"DriverNumber": 63,"Venue": "Sakhir","Name": "George Russell","Abbreviation": "RUS","Team": "Mercedes","Q1": 54.160,"Q2": 53.819,"Q3": 53.403},
        {"Year": 2020,"Position": 3,"DriverNumber": 33,"Venue": "Sakhir","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": 54.037,"Q2": 53.647,"Q3": 53.433},
        {"Year": 2020,"Position": 4,"DriverNumber": 16,"Venue": "Sakhir","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": 54.249,"Q2": 53.825,"Q3": 53.613},
        {"Year": 2020,"Position": 5,"DriverNumber": 11,"Venue": "Sakhir","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": 54.236,"Q2": 53.787,"Q3": 53.790},
        {"Year": 2020,"Position": 6,"DriverNumber": 26,"Venue": "Sakhir","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": 54.346,"Q2": 53.856,"Q3": 53.906},
        {"Year": 2020,"Position": 7,"DriverNumber": 3,"Venue": "Sakhir","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": 54.388,"Q2": 53.871,"Q3": 53.957},
        {"Year": 2020,"Position": 8,"DriverNumber": 55,"Venue": "Sakhir","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": 54.450,"Q2": 53.818,"Q3": 54.010},
        {"Year": 2020,"Position": 9,"DriverNumber": 10,"Venue": "Sakhir","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": 54.207,"Q2": 53.941,"Q3": 54.154},
        {"Year": 2020,"Position": 10,"DriverNumber": 18,"Venue": "Sakhir","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": 54.595,"Q2": 53.840,"Q3": 54.200},
        {"Year": 2020,"Position": 11,"DriverNumber": 31,"Venue": "Sakhir","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": 54.309,"Q2": 53.995,"Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 23,"Venue": "Sakhir","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": 54.620,"Q2": 54.026,"Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 5,"Venue": "Sakhir","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": 54.301,"Q2": 54.175,"Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 99,"Venue": "Sakhir","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": 54.523,"Q2": 54.377,"Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 4,"Venue": "Sakhir","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": 54.194,"Q2": 54.693,"Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 20,"Venue": "Sakhir","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": 54.705,"Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 6,"Venue": "Sakhir","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": 54.796,"Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 89,"Venue": "Sakhir","Name": "Jack Aitken","Abbreviation": "AIT","Team": "Williams Mercedes","Q1": 54.892,"Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 7,"Venue": "Sakhir","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": 54.963,"Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 51,"Venue": "Sakhir","Name": "Pietro Fittipaldi","Abbreviation": "FIT","Team": "Haas Ferrari","Q1": 55.426,"Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 1,"DriverNumber": 33,"Venue": "Abu-Dhabi","Name": "Max Verstappen","Abbreviation": "VER","Team": "Red Bull Racing Honda","Q1": "1:35.993","Q2": "1:35.641","Q3": "1:35.246"},
        {"Year": 2020,"Position": 2,"DriverNumber": 77,"Venue": "Abu-Dhabi","Name": "Valtteri Bottas","Abbreviation": "BOT","Team": "Mercedes","Q1": "1:35.699","Q2": "1:35.527","Q3": "1:35.271"},
        {"Year": 2020,"Position": 3,"DriverNumber": 44,"Venue": "Abu-Dhabi","Name": "Lewis Hamilton","Abbreviation": "HAM","Team": "Mercedes","Q1": "1:35.528","Q2": "1:35.466","Q3": "1:35.332"},
        {"Year": 2020,"Position": 4,"DriverNumber": 4,"Venue": "Abu-Dhabi","Name": "Lando Norris","Abbreviation": "NOR","Team": "McLaren Renault","Q1": "1:36.016","Q2": "1:35.849","Q3": "1:35.497"},
        {"Year": 2020,"Position": 5,"DriverNumber": 23,"Venue": "Abu-Dhabi","Name": "Alexander Albon","Abbreviation": "ALB","Team": "Red Bull Racing Honda","Q1": "1:36.106","Q2": "1:35.654","Q3": "1:35.571"},
        {"Year": 2020,"Position": 6,"DriverNumber": 55,"Venue": "Abu-Dhabi","Name": "Carlos Sainz","Abbreviation": "SAI","Team": "McLaren Renault","Q1": "1:36.517","Q2": "1:36.192","Q3": "1:35.815"},
        {"Year": 2020,"Position": 7,"DriverNumber": 26,"Venue": "Abu-Dhabi","Name": "Daniil Kvyat","Abbreviation": "KVY","Team": "AlphaTauri Honda","Q1": "1:36.459","Q2": "1:36.214","Q3": "1:35.963"},
        {"Year": 2020,"Position": 8,"DriverNumber": 18,"Venue": "Abu-Dhabi","Name": "Lance Stroll","Abbreviation": "STR","Team": "Racing Point BWT Mercedes","Q1": "1:36.502","Q2": "1:36.143","Q3": "1:36.046"},
        {"Year": 2020,"Position": 9,"DriverNumber": 16,"Venue": "Abu-Dhabi","Name": "Charles Leclerc","Abbreviation": "LEC","Team": "Ferrari","Q1": "1:35.881","Q2": "1:35.932","Q3": "1:36.065"},
        {"Year": 2020,"Position": 10,"DriverNumber": 10,"Venue": "Abu-Dhabi","Name": "Pierre Gasly","Abbreviation": "GAS","Team": "AlphaTauri Honda","Q1": "1:36.545","Q2": "1:36.282","Q3": "1:36.242"},
        {"Year": 2020,"Position": 11,"DriverNumber": 31,"Venue": "Abu-Dhabi","Name": "Esteban Ocon","Abbreviation": "OCO","Team": "Renault","Q1": "1:36.783","Q2": "1:36.359","Q3": "N/A"},
        {"Year": 2020,"Position": 12,"DriverNumber": 3,"Venue": "Abu-Dhabi","Name": "Daniel Ricciardo","Abbreviation": "RIC","Team": "Renault","Q1": "1:36.704","Q2": "1:36.406","Q3": "N/A"},
        {"Year": 2020,"Position": 13,"DriverNumber": 5,"Venue": "Abu-Dhabi","Name": "Sebastian Vettel","Abbreviation": "VET","Team": "Ferrari","Q1": "1:36.655","Q2": "1:36.631","Q3": "N/A"},
        {"Year": 2020,"Position": 14,"DriverNumber": 99,"Venue": "Abu-Dhabi","Name": "Antonio Giovinazzi","Abbreviation": "GIO","Team": "Alfa Romeo Racing Ferrari","Q1": "1:37.075","Q2": "1:38.248","Q3": "N/A"},
        {"Year": 2020,"Position": 15,"DriverNumber": 11,"Venue": "Abu-Dhabi","Name": "Sergio Perez","Abbreviation": "PER","Team": "Racing Point BWT Mercedes","Q1": "1:36.034","Q2": "DNS","Q3": "N/A"},
        {"Year": 2020,"Position": 16,"DriverNumber": 7,"Venue": "Abu-Dhabi","Name": "Kimi Raikkonen","Abbreviation": "RAI","Team": "Alfa Romeo Racing Ferrari","Q1": "1:37.555","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 17,"DriverNumber": 20,"Venue": "Abu-Dhabi","Name": "Kevin Magnussen","Abbreviation": "MAG","Team": "Haas Ferrari","Q1": "1:37.863","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 18,"DriverNumber": 63,"Venue": "Abu-Dhabi","Name": "George Russell","Abbreviation": "RUS","Team": "Williams Mercedes","Q1": "1:38.045","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 19,"DriverNumber": 51,"Venue": "Abu-Dhabi","Name": "Pietro Fittipaldi","Abbreviation": "FIT","Team": "Haas Ferrari","Q1": "1:38.173","Q2": "N/A","Q3": "N/A"},
        {"Year": 2020,"Position": 20,"DriverNumber": 6,"Venue": "Abu-Dhabi","Name": "Nicholas Latifi","Abbreviation": "LAT","Team": "Williams Mercedes","Q1": "1:38.443","Q2": "N/A","Q3": "N/A"}];

        // Data taken from https://www.kaggle.com/aadiltajani/fia-f1-19502019-data?select=constructors_championship_1958-2020.csv  -- Qualifying laps data

      var teamColors={
        "Mercedes" : "#00D2BE",
        "Red Bull Racing Honda" : "#1E41FF",
        "McLaren Renault" : "#FF8700",
        "AlphaTauri Honda" : "#FFFFFF",
        "Racing Point BWT Mercedes" : "#F596C8",
        "Ferrari" : "#C80000",
        "Renault" : "#FFF500",
        "Haas Ferrari" : "#787878",
        "Alfa Romeo Racing Ferrari" : "#9B0000",
        "Williams Mercedes" : "#0082FA"
      };
      var margin = {top: 7.5, bottom: 160, left:20, right: 80};
      var width = window.screen.width+1.5;
      var height = window.screen.height-margin.bottom - margin.top - 130;
      let inputVenue;
      const venues = getVenues(data);

      grandsPrixDiv = document.getElementById("grands-prix-div");

      for(entry in venues){
        let venue = document.createElement('span');
        venue.textContent =`${venues[entry]}`;
        venue.className="grands-prix-text";
        venue.onclick = function(){
          inputVenue=this.textContent;
          document.getElementById("inputSession").value = 1;
          createField();
        }
        grandsPrixDiv.appendChild(venue);
      }

      function createField(){
        let qualifyingSession = parseInt(document.getElementById("inputSession").value);

        if(typeof inputVenue==='undefined'){
          inputVenue="Austria";
        }

        if(qualifyingSession>3 || qualifyingSession<=0 || isNaN(qualifyingSession)){
            qualifyingSession=1;
            document.getElementById("inputSession").value=qualifyingSession;
          }

        let qualificationData = [];
        
        qualificationData = sortByQualification(getQualificationData(inputVenue),qualifyingSession);
        document.getElementById("qualifying-session").textContent = `${inputVenue} Q${qualifyingSession}`;

        d3.select("svg").remove();

        var svg = d3.select("body")
          .append("svg")
          .attr("width",width)
          .attr("height",height+45)
          .style("background-color","#282B2A");

        var upperChicane = svg.selectAll("image.upperchicane")
          .data([...Array(20).keys()])
          .enter()
          .append("svg:image")
          .attr("x",function(d,i){return (i)*width/20;})
          .attr("y", function(d,i){ return -5;})
          .attr("class","image-chicane")
          .attr("xlink:href", "res/chicane.jpg");

        var bottomChicane = svg.selectAll("image.bottomChicane")
          .data([...Array(20).keys()])
          .enter()
          .append("svg:image")
          .attr("x",function(d,i){return (i)*width/20;})
          .attr("y", function(d,i){ return height + 30;})
          .attr("class","image-chicane")
          .attr("xlink:href", "res/chicane.jpg");

        var names = svg.selectAll("text.names")
          .data(qualificationData)
          .enter()
          .append("text")
          .text(function(d,i){ return d.Abbreviation; })
          .attr("x",function(d){ return 65 + margin.left; })
          .attr("y", function(d,i){ return 50 + i*height/qualificationData.length; })
          .attr("class","names");

        var teamRects = svg.selectAll("rect.team")
          .data(qualificationData)
          .enter()
          .append("rect")
          .attr("x",function(d){return margin.left + 45;})
          .attr("y", function(d,i){ return 30 + (i)*height/qualificationData.length})
          .attr("width","10")
          .attr("height","30")
          .attr("style",function(d){return `stroke:black;stroke-width:0.25;fill:${teamColors[d.Team]};`});

        var driverStanding = svg.selectAll("text.standings")
            .data(qualificationData)
            .enter()
            .append("text")
            .text(function(d,i){return i+1;})
            .attr("x",function(d){ return 15 + margin.left; })
            .attr("y", function(d,i){ return 51.25 + i*height/qualificationData.length; })
            .attr("class","names")
            .attr("text-anchor","middle");

        var checkeredFlag = svg.selectAll("image.flags")
          .data([...Array(19).keys()])
          .enter()
          .append("svg:image")
          .attr("x",function(d){return  width-310;})
          .attr("y", function(d,i){ return 15 +(i)*height/19.75})
          .attr("class","image-flag")
          .attr("xlink:href", "res/flag.png");
        
        var cars = svg.selectAll("image.cars")
          .data(qualificationData)
          .enter()
          .append("svg:image")
          .attr("x",function(d){return  145 + margin.left;})
          .attr("y", function(d,i){ return 15 + (i)*height/qualificationData.length})
          .attr('width',  175 + (1 - qualificationData.length/20) * 50)
          .attr('height', 40 + (1 - qualificationData.length/20) * 40)
          .attr("xlink:href", function(d){return `res/cars/${d.Team}.png`});
        
        cars.transition()
          .duration(function(d,i){ return 2000 + 2000 * getTimeDifference(d["Q"+qualifyingSession],qualificationData[0]["Q"+qualifyingSession]); })
          .attr("transform", function(d){ 
            if(d["Q"+qualifyingSession]==="DNF" || d["Q"+qualifyingSession]==="DNS" || d["Q"+qualifyingSession]==="N/A"){
              return "translate(0,0)";
            }
            return `translate(${width - margin.right - 370},0)`; 
          }); 

          var timeDifferences = svg.selectAll("text.times")
            .data(qualificationData)
            .enter()
            .append("text")
            .text(function(d,i){ 
              return timeDifferenceToString(getTimeDifference(d["Q"+qualifyingSession],qualificationData[0]["Q"+qualifyingSession])); })
            .attr("x",function(d){ return width - margin.right; })
            .attr("y", function(d,i){ return 45 + i*height/qualificationData.length; })
            .attr("class","names")
            .style("clear","right");

      }

      function getVenues(data){
        let venues=[]
        for(entry in data){
          if(!venues.includes(data[entry].Venue)){
            venues.push(data[entry].Venue);
          }
        }
        return venues;
      }
      

      function sortByQualification(data,qualifyingSession){
        let driversLeft=[];

        if(qualifyingSession===1){
          return data.sort(function (x, y) { return getFloatTime(x.Q1) - getFloatTime(y.Q1); });
        }
        else if(qualifyingSession===2){
          data.sort(function (x, y) { return getFloatTime(x.Q2) - getFloatTime(y.Q2); });
          driversLeft = data.slice(0,15);
          return driversLeft;
        }
        else if(qualifyingSession===3){
          data.sort(function (x, y) { return getFloatTime(x.Q3) - getFloatTime(y.Q3); });
          driversLeft = data.slice(0,10);
          return driversLeft;
        }
      }

      function getQualificationData(venue){
        let qualificationData=[];
        for(entry in data){
          if(data[entry].Venue == venue){
            qualificationData.push(data[entry]);
          }
        }
        return qualificationData;
      }

      function getTimeDifference(lap_1,lap_2){
        if(lap_1==="DNS" || lap_1==="DNF"){
          return lap_1;
        }
        if(lap_1==="N/A"){
          return "No time";
        }
        let time_1 = getFloatTime(lap_1);
        let time_2 = getFloatTime(lap_2);
        let roundedTimeDifference = toFixed(time_1-time_2,4);

        return roundedTimeDifference;
      }

      function timeDifferenceToString(timeDifference){
          if(timeDifference>0){
            return "+" + timeDifference;
          }
          else if(timeDifference===0){
            return "Leader";
          }
          return String(timeDifference);
      }

      function getFloatTime(lap){
        if(isString(lap)){
          let sTime = lap.split(":");
          let time = parseInt(sTime[0]*60) + parseFloat(sTime[1]);
          return time;
        }
        return lap;
      }

      function isString(input){
        if (typeof input === 'string' || input instanceof String){
          return true;
        }
        return false;
      }

      function toFixed(value, precision) {
        var power = Math.pow(10, precision || 0);
        return Math.round(value * power) / power;
      }